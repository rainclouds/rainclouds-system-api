# RainClouds System API
RainClouds System API is used for the management and organization of data spread across a cloud.
The API consolidates data from any number of sources specified by the end user which allows them to treat their
cloud as though it were a local filesystem.

## Everything, Everywhere, All In One Place (EEAP)
RainClouds follows a philosophy of design titled "Everything, Everywhere, All in one Place", or EEAP for short. EEAP is a philosophy of design that attempts to treat data spread across several devices within a network as though it were all in one place.