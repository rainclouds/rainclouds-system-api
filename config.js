const util = require('util');


module.exports = {
    system_conn_str: util.format("%s%s:%s@%s/?retryWrites=true&w=majority/%s",
        process.env.SYSTEM_DB_PROTOCOL,
        process.env.SYSTEM_DB_USERNAME,
        process.env.SYSTEM_DB_PASSWORD,
        process.env.SYSTEM_DB_HOST,
        process.env.SYSTEM_DB_NAME)
}