const mongoose = require("mongoose");
const TreeNode = require("../models/filesystem/TreeNode");

module.exports = (app, root_path) => {
    const localRoot = root_path + '/filesystem';

    // Storage Pool - an object that contains a collection of remote volumes

    // List Storage Pools
    app.get(localRoot + '/', (req, res) => {

    });

    // List directories by nodeId
    app.get(localRoot + '/list/:nodeId', async (req, res, next) => {
        const nodeId = new mongoose.Types.ObjectId(req.params.nodeId);
        if(!await TreeNode.exists({"_id": nodeId}))
            return res.status(500).json({status: 404, message: "Directory not found!"});
        return res.status(200).json({
            "children": await TreeNode.find({"parentNode": nodeId})
        });
    });

    // Create directory
    app.post(localRoot + '/mkdir', async (req, res, next) => {

    });

    // List directories within storage pool
    app.get(localRoot + '/list', (req, res) => {
        const path = req.query.path;
        if(!path) {
            throw {status: 400, message: 'Invalid Request!'};
        }
        res.json({"path": req.query.path});
    });
}