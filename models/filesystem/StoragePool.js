import mongoose from 'mongoose';

class StoragePool extends mongoose.Schema {
    constructor() {
        return super({
            name: {
                type: String,
                unique: true,
                required: true
            },
            mountPoint: {
                type: String,
                unique: true,
                required: true
            },
            volumes: [{
                _volume: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'volume'
                }
            }]
        });
    }
}

export default mongoose.model('storagepool', new storagePool)