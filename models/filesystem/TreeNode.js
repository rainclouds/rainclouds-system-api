const mongoose = require('mongoose');


class TreeNode extends mongoose.Schema {
    constructor() {
        return super({
            nodeName: {
                type: String,
                unique: true,
                required: true
            },
            parentNode: {
                type: mongoose.Schema.Types.ObjectId,
                unique: false,
                required: true,
                index: true
            },
            nodeType: {
                type: String,
                enum: ['file', 'directory', 'symlink', 'mountpoint'],
                required: true
            }
        });
    }
};
module.exports = mongoose.model('filetree', new TreeNode)