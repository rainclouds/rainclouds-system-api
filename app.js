/* LOAD ENVIRONMENT VARIABLES */
require('dotenv').config();

/* External packages */
const express = require('express');
const fs = require('fs');
const path = require('path');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const util = require('util');

/* Internal packages */
const conf = require('./config');

const app = express();
const ROOT_PATH = '/api';
const router = express.Router();



/* SETUP ROUTER */
fs.readdirSync(__dirname + '/routes').forEach(
    (file) => {
        module.exports[path.basename(file, '.js')] =
            require(path.join(__dirname + '/routes', file))(app, ROOT_PATH);
    }
);

/* INDEX ROUTE */
router.get('/', (req, res) => {
    res.json({'status': 'online'});
});

/* SETUP ERROR HANDLING */
app.use((err, req, res, next) => {
    res.status(err.status || 500).send({status: err.status, message: err.message})
});

const main = async () => {
    /* Configure Mongoose connection */
    await mongoose.connect(conf.system_conn_str);

    /* Setup JSON Parser */
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());

    /* Start RainClouds System API web service */
    const port = process.env.SERVICE_PORT || 3000;
    const hostName = process.env.SERVICE_HOSTNAME || "0.0.0.0";
    app.use(ROOT_PATH, router).listen(port, hostName);
    console.log("cloud-system-api listening on ", util.format("http://%s:%s%s", hostName, port, ROOT_PATH))
}

main().then((text) => {
    console.log("cloud-system-api service has successfully started!");
}).catch((err) => {
    console.error("Tried launching cloud-system-api service but caught errors in the process...");
    console.error(JSON.stringify(err, ["message", "arguments", "type", "name"]));
});
